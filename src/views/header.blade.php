<div class="app-header" data-uk-sticky="{animation: 'uk-animation-slide-top', showup:true}" >
	<div class="app-header-topbar" >
		<div class="uk-container uk-container-center" >
			<div class="uk-grid uk-flex-middle" >
				<div >
					<div class="uk-display-inline-block" data-uk-dropdown="delay:400" >
						<a href="/fellow_repo/cms/" class="uk-link-muted uk-text-bold" >
							<i class="uk-icon-bars" ></i >
							<span class="app-name" >Fellow Review</span >
						</a >
						<div class="uk-dropdown app-panel-dropdown" >
							<div class="uk-grid uk-grid-gutter uk-grid-small uk-grid-divider" >
								<div class="uk-width-medium-1-3" >
									<div class="uk-margin" >
										<span class="uk-badge uk-badge-primary" >System</span >
									</div >
									<ul class="uk-nav uk-nav-side uk-nav-dropdown app-nav" >
										<li class="" >
											<a href="/fellow_repo/cms/cockpit/dashboard" >
												<img class="uk-margin-small-right inherit-color" src="/fellow_repo/cms/assets/app/media/icons/dashboard.svg" width="30" height="30" data-uk-svg alt="assets" />
												Dashboard
											</a >
										</li >
										<li class="" >
											<a href="/fellow_repo/cms/assetsmanager" >
												<img class="uk-margin-small-right inherit-color" src="/fellow_repo/cms/assets/app/media/icons/assets.svg" width="30" height="30" data-uk-svg alt="assets" />
												Assets
											</a >
										</li >
										<li class="uk-nav-divider" ></li >
										<li class="" >
											<a href="/fellow_repo/cms/accounts" >
												<img class="uk-margin-small-right inherit-color" src="/fellow_repo/cms/assets/app/media/icons/accounts.svg" width="30" height="30" data-uk-svg alt="assets" />
												Accounts
											</a >
										</li >
										<li class="uk-nav-divider" ></li >
										<li class="" >
											<a href="/fellow_repo/cms/finder" >
												<img class="uk-margin-small-right inherit-color" src="/fellow_repo/cms/assets/app/media/icons/finder.svg" width="30" height="30" data-uk-svg alt="assets" />
												Finder
											</a >
										</li >
										<li class="" >
											<a href="/fellow_repo/cms/settings" >
												<img class="uk-margin-small-right inherit-color" src="/fellow_repo/cms/assets/app/media/icons/settings.svg" width="30" height="30" data-uk-svg alt="assets" />
												Settings
											</a >
										</li >
									</ul >
									<ul class="uk-nav uk-nav-side uk-nav-dropdown uk-margin-top" >
										<li class="uk-nav-header" >Collections</li >
										<li >
											<a href="/fellow_repo/cms/collections/entries/User" >
												<i class="uk-icon-justify uk-icon-list" ></i >
												User
											</a >
										</li >
									</ul >
								</div >
								<div class="uk-grid-margin uk-width-medium-2-3" >
									<div class="uk-margin" >
										<span class="uk-badge uk-badge-primary" >Modules</span >
									</div >
									<ul class="uk-sortable uk-grid uk-grid-match uk-grid-small uk-grid-gutter uk-text-center" data-modules-menu data-uk-sortable >
										<li class="uk-width-1-2 uk-width-medium-1-3" data-route="/collections" >
											<a class="uk-display-block uk-panel-box uk-panel-framed" href="/fellow_repo/cms/collections" >
												<div class="uk-svg-adjust" >
													<img src="/fellow_repo/cms/modules/Collections/icon.svg" alt="Collections" data-uk-svg width="30px" height="30px" />
												</div >
												<div class="uk-text-truncate uk-text-small uk-margin-small-top" >Collections</div >
											</a >
										</li >
									</ul >
								</div >
							</div >
						</div >
					</div >
				</div >
				<div class="uk-flex-item-1" riot-mount >
					<cp-search ></cp-search >
				</div >
				<div class="uk-hidden-small" >
					<ul class="uk-subnav app-modulesbar" >
						<li >
							<a class="uk-svg-adjust " href="/fellow_repo/cms/collections" title="Collections" data-uk-tooltip="offset:10" >
								<img src="/fellow_repo/cms/modules/Collections/icon.svg" alt="Collections" data-uk-svg width="20px" height="20px" />
							</a >
						</li >
					</ul >
				</div >
				<div >
					<div data-uk-dropdown="delay:150" >
						<a class="uk-display-block" href="/fellow_repo/cms/accounts/account" style="width:30px;height:30px;" riot-mount >
							<cp-gravatar email="admin@yourdomain.de" size="30" alt="Admin" ></cp-gravatar >
						</a >
						<div class="uk-dropdown uk-dropdown-navbar uk-dropdown-flip" >
							<ul class="uk-nav uk-nav-navbar" >
								<li class="uk-nav-header uk-text-truncate" >Admin</li >
								<li >
									<a href="/fellow_repo/cms/accounts/account" >Account</a >
								</li >
								<li class="uk-nav-divider" ></li >
								<li >
									<a href="/fellow_repo/cms/auth/logout" >Logout</a >
								</li >
							</ul >
						</div >
					</div >
				</div >
			</div >
		</div >
	</div >
</div >
