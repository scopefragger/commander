@extends('admin::layout')
@section('content')
	<div class="app-main">
		<div class="uk-container uk-container-center">

			<style>
				.app-header { border-top: 8px #D8334A solid; }
			</style>


			<div>

				<ul class="uk-breadcrumb">
					<li><a href="/fellow_repo/cms/collections">Collections</a></li>
					<li class="uk-active" data-uk-dropdown="mode:'hover', delay:300">

						<a><i class="uk-icon-bars"></i> Education</a>

						<div class="uk-dropdown">
							<ul class="uk-nav uk-nav-dropdown">
								<li class="uk-nav-header">Actions</li>
								<li><a href="/fellow_repo/cms/collections/collection/Education">Edit</a></li>
								<li class="uk-nav-divider"></li>
								<li class="uk-text-truncate"><a href="/fellow_repo/cms/collections/export/Education" download="Education.collection.json">Export entries</a></li>
								<li class="uk-text-truncate"><a href="/fellow_repo/cms/collections/import/collection/Education">Import entries</a></li>
							</ul>
						</div>

					</li>
				</ul>

			</div>

			<div class="uk-margin uk-text-center uk-text-muted">

				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.6 511.6" style="enable-background:new 0 0 511.6 511.6;" xml:space="preserve" class="uk-svg-adjust" width="50">
<rect x="77.47" y="378.16" style="fill:#F5F7FA;" width="359.52" height="73.27"></rect>
					<path style="fill:#CCD1D9;" d="M191.85,437.002h-85.277c-5.886,0-10.647-4.777-10.647-10.663c0-5.887,4.762-10.664,10.647-10.664  h85.277c5.886,0,10.647,4.777,10.647,10.664C202.497,432.225,197.736,437.002,191.85,437.002z"></path>
					<path style="fill:#434A54;" d="M106.572,405.028h330.414c5.886,0,10.664-4.778,10.664-10.664v-383.7  c0-5.886-4.778-10.663-10.664-10.663H106.572c-23.544,0-42.623,19.094-42.623,42.639c0,0,0,0,0,0.016v351.709  c0,0.078,0,0.141,0,0.219l0,0v31.757c0,23.544,19.079,42.638,42.623,42.638h330.414l0,0c5.886,0,10.664-4.777,10.664-10.663  s-4.778-10.663-10.664-10.663h-0.016H106.572c-11.741,0-21.312-9.556-21.312-21.312C85.261,414.582,94.831,405.028,106.572,405.028z  "></path>
					<path style="fill:#656D78;" d="M106.572,405.028H127.9V0h-21.327C83.03,0,63.951,19.094,63.951,42.639c0,0,0,0,0,0.016v351.709  c0,0.078,0,0.141,0,0.219l0,0v31.757c0,23.544,19.079,42.638,42.623,42.638H127.9v-21.326h-21.327  c-11.741,0-21.312-9.556-21.312-21.312C85.261,414.582,94.831,405.028,106.572,405.028z"></path>
					<polygon style="fill:#ED5564;" points="191.85,511.6 149.211,490.288 106.572,511.6 106.572,426.339 191.85,426.339 "></polygon>
					<polygon style="fill:#FFCE54;" points="277.112,120.421 303.465,173.802 362.373,182.373 319.75,223.919 329.805,282.608   277.112,254.895 224.418,282.608 234.473,223.919 191.85,182.373 250.757,173.802 "></polygon>
					<g>
						<path style="fill:#CCD1D9;" d="M436.987,426.339c-5.887,0-10.664,4.777-10.664,10.663s4.777,10.648,10.664,10.648V426.339z"></path>
						<path style="fill:#CCD1D9;" d="M436.987,405.028L436.987,405.028c-5.887,0-10.664,4.762-10.664,10.647s4.777,10.664,10.664,10.664   l0,0V405.028z"></path>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
</svg>
			</div>

			<div class="uk-margin-top" data-is="view-f9963233"><div>   <div class="uk-clearfix uk-margin-top"> <div class="uk-float-left uk-width-1-2"> <div class="uk-form-icon uk-form uk-width-1-1 uk-text-muted"> <i class="uk-icon-search"></i> <input class="uk-width-1-1 uk-form-large uk-form-blank" type="text" ref="txtfilter" placeholder="Filter items..."> </div> </div> <div class="uk-float-right">  <a class="uk-button uk-button-large uk-button-primary" href="/fellow_repo/cms/collections/entry/Education"><i class="uk-icon-plus-circle uk-icon-justify"></i> Entry</a> </div> </div> <div class="uk-margin-top">  <table class="uk-table uk-table-border uk-table-striped"> <thead> <tr> <th width="20"><input type="checkbox" data-check="all"></th> <th class="uk-text-small"> <a class="uk-link-muted " data-sort="grade"> grade  </a> </th><th class="uk-text-small"> <a class="uk-link-muted " data-sort="Place Of Education"> Place Of Education  </a> </th><th class="uk-text-small"> <a class="uk-link-muted " data-sort="Course"> Course  </a> </th><th class="uk-text-small"> <a class="uk-link-muted " data-sort="date-started"> date-started  </a> </th><th class="uk-text-small"> <a class="uk-link-muted " data-sort="date-ended"> date-ended  </a> </th><th class="uk-text-small" width="120"> <a class="uk-link-muted " data-sort="_modified"> Modified  </a> </th> <th width="20"></th> </tr> </thead> <tbody> <tr> <td><input type="checkbox" data-check="" data-id="592453ac366f075cf52a65d1"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1"> <raw content="neveryoumind">neveryoumind</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1"> <raw content="durham">durham</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1"> <raw content="business">business</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1"> <raw content="05/14/2017">05/14/2017</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1"> <raw content="05/16/2017">05/16/2017</raw> </a> </td> <td class="uk-text-muted">May 23, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/592453ac366f075cf52a65d1">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="591c7488366f072c1b4b6471"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471"> <raw content="1st">1st</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471"> <raw content="oxford">oxford</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471"> <raw content="ict">ict</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471"> <raw content="05/24/2017">05/24/2017</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471"> <raw content="05/31/2017">05/31/2017</raw> </a> </td> <td class="uk-text-muted">May 17, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/591c7488366f072c1b4b6471">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="591c4f48366f0777474d3491"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491"> <raw content="1st hons">1st hons</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491"> <raw content="Ljmu">Ljmu</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491"> <raw content="Liverpool">Liverpool</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491"> <raw content="05/01/2017">05/01/2017</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491"> <raw content="05/02/2017">05/02/2017</raw> </a> </td> <td class="uk-text-muted">May 17, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/591c4f48366f0777474d3491">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="591ada67366f070d2b6ee091"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091"> <raw content="2345">2345</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091"> <raw content="1234">1234</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091"> <raw content="2345">2345</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091"> <raw content="05/16/2017">05/16/2017</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091"> <raw content="05/16/2017">05/16/2017</raw> </a> </td> <td class="uk-text-muted">May 16, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/591ada67366f070d2b6ee091">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="591ada5c366f070d152a0271"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271"> <raw content="2345">2345</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271"> <raw content="1234">1234</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271"> <raw content="2345">2345</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271"> <raw content="05/16/2017">05/16/2017</raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271"> <raw content="05/16/2017">05/16/2017</raw> </a> </td> <td class="uk-text-muted">May 16, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/591ada5c366f070d152a0271">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="58f8c7b7366f0710ff635b22"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22"> <raw></raw> </a> </td> <td class="uk-text-muted">Apr 20, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b22">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr><tr> <td><input type="checkbox" data-check="" data-id="58f8c7b7366f0710ff635b23"></td> <td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23"> <raw></raw> </a> </td><td class="uk-text-truncate"> <a class="uk-link-muted" href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23"> <raw></raw> </a> </td> <td class="uk-text-muted">Apr 20, 2017</td> <td> <span data-uk-dropdown="mode:'click'"> <a class="uk-icon-bars"></a> <div class="uk-dropdown uk-dropdown-flip"> <ul class="uk-nav uk-nav-dropdown"> <li class="uk-nav-header">Actions</li> <li><a href="/fellow_repo/cms/collections/entry/Education/58f8c7b7366f0710ff635b23">Edit</a></li> <li><a class="uk-dropdown-close">Delete</a></li> <li class="uk-nav-divider"></li> <li><a class="uk-dropdown-close">Duplicate</a></li> </ul> </div> </span> </td> </tr> </tbody> </table>   </div> </div></div>
		</div>
	</div>
@endsection