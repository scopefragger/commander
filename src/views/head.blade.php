<head >
	<meta charset="UTF-8" >
	<title >Fellow Review</title >
	<link rel="icon" href="http://dev.e3creative.co.uk/fellow_repo/cms/favicon.ico" type="image/x-icon" >
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<script >
		// App constants
		var SITE_URL = '/fellow_repo';
		var ASSETS_URL = 'http://dev.e3creative.co.uk/fellow_repo/cms/storage/uploads';
	</script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/fuc.js.php" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/polyfills/es6-shim.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/polyfills/dom4.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/polyfills/fetch.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/polyfills/web-animations.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/polyfills/pointer-events.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/moment.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/jquery.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/lodash.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/riot/riot.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/riot/riot.bind.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/riot/riot.view.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/uikit.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/notify.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/tooltip.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/lightbox.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/sortable.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/sticky.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/mousetrap.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/storage.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/i18n.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/app/js/app.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/app/js/app.utils.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/modules/Cockpit/assets/components.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/modules/Cockpit/assets/cockpit.js?ver=0.3.1" type="text/javascript" ></script >
	<link href="http://dev.e3creative.co.uk/fellow_repo/cms/assets/app/css/style.css?ver=0.3.1" type="text/css" rel="stylesheet" >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/autocomplete.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/lib/uikit/js/components/tooltip.min.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/assets/app/js/bootstrap.js?ver=0.3.1" type="text/javascript" ></script >
	<script src="http://dev.e3creative.co.uk/fellow_repo/cms/cockpit.i18n.data" ></script >
	<script >
		App.$data = {
			"user": {
				"user": "admin",
				"email": "admin@yourdomain.de",
				"group": "admin",
				"_id": "58f8ca27366f0713b564c701",
				"name": "Admin",
				"active": true,
				"i18n": "en",
				"data": {}
			}, "locale": null, "site_url": "\/fellow_repo\/", "languages": [], "groups": {"admin": true}
		};
	</script >
</head >