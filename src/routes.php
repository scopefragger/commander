<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Scopefragger\Commander\CommanderServiceProvider;

Route::get('/admin', 'Scopefragger\Commander\Controllers\AppController@dash');
Route::get('/admin/{function}/{tax}', 'Scopefragger\Commander\Controllers\LoaderController@manager');
