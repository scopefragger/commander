<?php

namespace Scopefragger\Commander;


use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class CommanderServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__.'/views', 'admin');

    }
}