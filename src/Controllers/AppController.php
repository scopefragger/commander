<?php

namespace Scopefragger\Commander\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\View;

class AppController extends Controller
{
    public function dash()
    {
        return view('admin::dash.index');
    }
}
