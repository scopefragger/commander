<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInita8dc4e29e4d62d20d6816ceec35a9f00
{
    public static $files = array (
        '8869d3a78059cc3163bda0a711797bc2' => __DIR__ . '/../..' . '/src/helpers.php',
    );

    public static $prefixLengthsPsr4 = array (
        'E' => 
        array (
            'E3Creative\\Translate\\' => 21,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'E3Creative\\Translate\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInita8dc4e29e4d62d20d6816ceec35a9f00::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInita8dc4e29e4d62d20d6816ceec35a9f00::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
